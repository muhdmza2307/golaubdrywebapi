﻿using goLaundryWebAPI.Data;
using goLaundryWebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Security.Cryptography;
using goLaundryWebAPI.Helpers;
using Newtonsoft.Json;
using System.Net;
using goLaundryWebAPI.Models.Request;

namespace goLaundryWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IConfiguration _iConfiguration;

        public AccountController(IConfiguration iConfiguration)
        {
            _iConfiguration = iConfiguration;
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public IActionResult Login([FromBody] AccRequest loginPass)
        {
            RespModel<UserDetails> resp = new RespModel<UserDetails>();

            try
            {
                DataTable dtResult = new AccountDataLayer(_iConfiguration).GetAccountLogin(loginPass.userName, loginPass.password);

                if (dtResult.Rows.Count < 1)
                {
                    return Ok(resp.Error(Convert.ToString(Convert.ToInt32(HttpStatusCode.OK)), "Bad Login."));
                }
                else
                {
                    UserDetails userDetails = new UserDetails();
                    userDetails = CommonHelper.GetObjectItem<UserDetails>(dtResult.Rows[0]);

                    //JWT Tokens
                    var now = DateTime.UtcNow;
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var secretKey = Encoding.ASCII.GetBytes(_iConfiguration["JWTSettings:Key"]);

                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[] {
                            new Claim(ClaimTypes.Email, userDetails.email)
                        }),
                        Expires = DateTime.UtcNow.AddMinutes(Double.Parse(_iConfiguration["JWTSettings:ExpMinute"])),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKey), SecurityAlgorithms.HmacSha256Signature)
                    };

                    var token = tokenHandler.CreateToken(tokenDescriptor);

                    userDetails.token = tokenHandler.WriteToken(token);
                    userDetails.tokenExpires = now.AddMinutes(Double.Parse(_iConfiguration["JWTSettings:ExpMinute"]));

                    resp.data = userDetails;
                    return Ok(resp);
                }

            }
            catch (Exception e)
            {
                return BadRequest(resp.Error(Convert.ToString(Convert.ToInt32(HttpStatusCode.BadRequest)), e.Message.ToString()));
            }
        }

    }
}
